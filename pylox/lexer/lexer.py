from dataclasses import dataclass
from typing import *
from enum import Enum, auto


class TokenType(Enum):
    T_Bang = auto()
    T_End = auto()
    T_Ident = auto()
    T_LBrace = auto()
    T_RBrace = auto()
    T_LParen = auto()
    T_RParen = auto()
    T_Comment = auto()
    T_Fn = auto()
    T_Var = auto()
    T_Plus = auto()
    T_Minus = auto()
    T_Mul = auto()
    T_Div = auto()
    T_Num = auto()
    T_NEq = auto()
    T_Eq = auto()
    T_LT = auto()
    T_GT = auto()
    T_GT_or_Eq = auto()
    T_LT_or_Eq = auto()
    T_Not = auto()
    T_Class = auto()
    T_Assign = auto()
    T_And = auto()
    T_While = auto()
    T_For = auto()
    T_Else = auto()
    T_If = auto()
    T_Or = auto()
    T_True = auto()
    T_False = auto()
    T_Ret = auto()
    T_Super = auto()
    T_Null = auto()
    T_Print = auto()
    T_This = auto()
    T_String = auto()



@dataclass
class Token:
    t_type: TokenType
    literal: str
    line: int


keywords = {
    "fn": TokenType.T_Fn,
    "var": TokenType.T_Var,
    "class": TokenType.T_Class,
    "for": TokenType.T_For,
    "if": TokenType.T_If,
    "not": TokenType.T_Not,
    "and": TokenType.T_And,
    "true": TokenType.T_True,
    "false": TokenType.T_False,
    "return": TokenType.T_Ret,
    "super": TokenType.T_Super,
    "or": TokenType.T_Or,
    "while": TokenType.T_While,
    "null": TokenType.T_Null,
    "print": TokenType.T_Print,
    "this": TokenType.T_This,
    "else": TokenType.T_Else
}


class Lexer:
    def __init__(self, source:str) -> None:
        self.source:str = source
        self.pos = 0
        self.errors = []
        self.line = 0

    def next_token(self) -> Token:
        # print(self.source, "From next_token function")

        if self.pos >= len(self.source):
            return Token(TokenType.T_End, "EOF", self.line)

        match self.curr_char():
            case '\n':
                self.line += 1
                self.advance()
            case ' ':
                self.advance()
            case '\t':
                self.advance()
            case '#':
                literal = self._consume_till_whitespace()
                self.advance(by=len(literal))
            # This should handle literals and keywords alike
            case '{':
                self.advance()
                return Token(TokenType.T_LBrace, '{',self.line)
            case '}':
                self.advance()
                return Token(TokenType.T_RBrace, '}',self.line)
            case '(':
                self.advance()
                return Token(TokenType.T_LParen, '(', self.line)
            case ')':
                self.advance()
                return Token(TokenType.T_RParen, ')', self.line)
            case '+':
                self.advance()
                return Token(TokenType.T_Plus, '+',self.line)
            case '-':
                self.advance()
                return Token(TokenType.T_Minus, '-', self.line)
            case '/':
                self.advance()
                return Token(TokenType.T_Div, '/',self.line)
            case '*':
                self.advance()
                return Token(TokenType.T_Star, '*',self.line)
            case '!':
                if self.peek_char() == '=':
                    self.advance(by=2)
                    return Token(TokenType.T_NEq, "!=" , self.line)
                self.advance()
                return Token(TokenType.T_Not, "!", self.line)
            case '=':
                if self.peek_char() == '=':
                    self.advance(by=2)
                    return Token(TokenType.T_Eq, "==", self.line)
                self.advance()
                return Token(TokenType.T_Assign, "=", self.line)
            case '<':
                if self.peek_char() == '=':
                    self.advance(2)
                    return Token(TokenType.T_LT_or_Eq, '<=', self.line)
                self.advance()
                return Token(TokenType.T_LT, '<', self.line)
            case '>':
                if self.peek_char() == '=':
                    self.advance(2)
                    return Token(TokenType.T_GT_or_Eq, '>=', self.line)
                self.advance()
                return Token(TokenType.T_GT, '>', self.line)

            case '"':
                string = self._string()
                self.advance(by=len(string)+2)
                return Token(TokenType.T_String, string, self.line)
            case _:
                if self.curr_char().isalpha() or self.curr_char() == "_":
                    literal = self._consume_till_whitespace()
                    self.advance(len(literal))
                    if literal in keywords:
                        return Token(keywords[literal], literal, self.line)
                    return Token(TokenType.T_Ident, literal, self.line)

                if self.curr_char().isdigit():
                    literal = self._consume_only_digits()
                    self.advance(by=len(literal))
                    return Token(TokenType.T_Num, int(literal), self.line)
                

    def _string(self):
        j = self.pos
        while j+1 < len(self.source)-1 and self.source[j+1] != '"' :
            j += 1
        if j+1<len(self.source) and self.source[j+1] != '"':
            raise Exception("Unterminated String Literal")
        string = self.source[self.pos+1:j+1]
        return string

    def _consume_till_whitespace(self): 
        j = self.pos
        while (j+1) < len(self.source) and self.source[j+1].isalpha() and (self.source[j+1] != ' ' and self.source[j+1] != '\n'):
            j += 1
        # print(self.source[self.pos:j], "from consume till whitespace", j)
        return self.source[self.pos:j+1]
    def consume_till_newline(self):
        j = self.pos
        while (j+1) < len(self.source) and self.source[j+1] != '\n':
            j += 1
        # print(self.source[self.pos:j], "from consume till whitespace", j)
        return self.source[self.pos:j+1]

    def advance(self, by=0):
        if by == 0:
            self.pos += 1
        else:
            self.pos += by

    def curr_char(self):
        return self.source[self.pos]

    def _consume_whitespace() -> None:
        pass
    def _consume_only_digits(self):
        j = self.pos
        while (j+1) < len(self.source) and self.source[j+1].isdigit() and (self.source[j+1] != ' ' or self.source[j+1] != '\n'):
            j += 1
        if (j+2 < len(self.source)-1) :
            if self.source[j+2] != ' ' or self.source[j+2] != '\n' or self.source[j+2] != '\t':
                self.errors.append(f"digits must not contain characters at {self.line}")
        return self.source[self.pos:j+1]

    def peek_char(self):
        if  self.pos < len(self.source)-1:
            return self.source[self.pos+1]
        return -1

    def get_all_tokens(self) -> List[Token]:
        out = []
        while True:
            token = self.next_token()
            # print(token)
            if token != None and token.t_type != TokenType.T_End:
                out.append(token)
            elif token != None and token.t_type == TokenType.T_End:
                out.append(token)
                break
        return out
