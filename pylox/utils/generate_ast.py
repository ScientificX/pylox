#!/usr/bin/env python3
import sys
import os

def generate_ast():
    if len(sys.argv) < 2:
        print("Please supply a directory argument" )
        exit(1)
    path = sys.argv[1]
    if not os.path.isdir(path):
        print("The supplied argument is not a directory")
        exit(1)
    fileNames = []
    subClasses = {
        "Binary": ["left", "op", "right"],
        "Grouping": ["expression"],
        "Unary": ["op", "right",],
        "Literal": ["value"]
    }
    def writeFile(directory, fileName, subClasses):
        filePath = directory + "/" + fileName
        f = open(filePath, 'w')
        for k, v in subClasses.items():
            f.write(f"class {k}: \n")        
            f.write(f'    def __init__(self, {",".join(v)}): \n')
            f.write(f'        pass \n')
        return
    writeFile(path, "Expr.py", subClasses)


if __name__ == "__main__":
    generate_ast()