from typing import List
from lexer.lexer import Token,Lexer,TokenType
from ast_printer import *

class PyserError(Exception):
    pass

class Pyser:
    def __init__(self, tokens=[]) -> None:
        self._tokens: List[Token] = tokens
        self._current = 0        
        print(tokens)

    def expression(self):
        return self.equality()

    def equality(self,):
        expr = self.comparison()
        
        while self.match([TokenType.T_NEq, TokenType.T_Eq]) and not self.isAtEnd():
            operator = self.previous()
            right = self.comparison()
            expr = Expr.Binary(expr , operator, right)

        return expr


    def comparison(self,):
        expr = self.term()
        while self.match([TokenType.T_LT, TokenType.T_GT, TokenType.T_GT_or_Eq, TokenType.T_LT_or_Eq ]):
            operator = self.previous()
            right = self.term()
            expr = Expr.Binary(expr, operator, right)
        return expr

    def term(self):
        expr = self.factor()
        while self.match([TokenType.T_Minus, TokenType.T_Plus]):
            operator = self.previous()
            right = self.factor()
            expr = Expr.Binary(expr,operator, right)
        return expr

    def factor(self):
        expr = self.unary()
        while self.match([TokenType.T_Div, TokenType.T_Mul]):
            operator = self.previous()
            right = self.unary()
            expr = Expr.Binary(expr, operator, right)
        return expr

    def unary(self):
        if self.match([TokenType.T_Not, TokenType.T_Minus]):
            operator = self.previous()
            return Expr.Unary(operator, self.peek())
        return self.primary()

    def primary(self,):
        if self.match([TokenType.T_True]): return Expr.Literal(self.previous().literal)
        if self.match([TokenType.T_Null]): return Expr.Literal(self.previous().literal)
        if self.match([TokenType.T_False]): return Expr.Literal(self.previous().literal)

        if self.match([TokenType.T_Num, TokenType.T_String]):
            return Expr.Literal(self.previous().literal)

        if self.match([TokenType.T_LParen]):
            expr = self.expression()
            # self.advance()
            self.consume(TokenType.T_RParen, "Expected )")
            return Expr.Grouping(expr)

        raise self.error(self.peek(), "Expected an expression")

    def consume(self,tok_type,mssg):
        if self.isAtEnd(): raise self.error(tok_type, mssg)

        if self.check(tok_type):
            return self.advance()
        raise self.error(self.peek(), mssg)


    def error(self, token, mssg):
        self.print_error(token, mssg)
        return PyserError()

    def print_error(self,token, mssg):
        if token.t_type == TokenType.T_End:
            print(f"{token.line} at end {mssg} ")

        else:
            print(f"{token.literal} at {token.line} {mssg}")

    def match(self,types:TokenType=[]):
        for ty in types:
            if self.check(ty):
                self.advance()
                return True

        return False


    def check(self,ty):
        if self.isAtEnd(): return False
        return self.peek().t_type == ty

    def isAtEnd(self,):
        return self.peek().t_type == TokenType.T_End

    def advance(self):
        if not self.isAtEnd(): self._current += 1
        return self.previous()

    def peek(self, ):
        return self._tokens[self._current]
    
    def peekNextToken(self,): return self._tokens[self._current+1]

    def previous(self):
        return self._tokens[self._current-1]


    def parse(self):
        # try:
        return self.expression()
        # except Exception as e:
            # print(e)
            # return None