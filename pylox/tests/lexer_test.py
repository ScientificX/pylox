from lexer.lexer import Lexer, Token, TokenType
def test_comment():
    l = Lexer("#")
    assert l.get_all_tokens() == [Token(TokenType.T_Pound, "#")]
