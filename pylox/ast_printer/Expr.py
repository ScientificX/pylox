from abc import ABC

# This is the visitor interface that every visitor must implement
class ExprVisitor(ABC):
    def visitAssignExpr(self,  expr): ...
    def visitBinaryExpr(self,expr): ...
    def visitCallExpr(self, expr): ...
    def visitGetExpr(self, expr): pass
    def visitGroupingExpr(self, expr): pass
    def visitLiteralExpr(self, expr): pass
    def visitLogicalExpr(self, expr): pass
    def visitSetExpr(self, expr): pass
    def visitSuperExpr(self, expr): pass
    def visitThisExpr(self, expr): pass
    def visitUnaryExpr(self, expr): pass
    def visitVariableExpr(self, expr): pass

class Expr(ABC):
    def accept(self, visitor: ExprVisitor):
        pass

class Statement(ABC):
    def accept(self,):
        pass

class Binary(Expr): 
    def __init__(self, left,op,right): 
        self.left = left
        self.right = right
        self.op = op
    def accept(self, visitor):
        return visitor.visitBinaryExpr(self)

class Grouping(Expr): 
    def __init__(self, expression): 
        self.expression = expression
    def accept(self, visitor: ExprVisitor):
        return visitor.visitGroupingExpr(self)

class Unary(Expr): 
    def __init__(self, op,right): 
        self.op = op
        self.right = right
    def accept(self, visitor: ExprVisitor):
        return visitor.visitUnaryExpr(self)

class Literal(Expr): 
    def __init__(self, literal): 
        self.literal = literal
    def accept(self, visitor: ExprVisitor):
        return visitor.visitLiteralExpr(self)
