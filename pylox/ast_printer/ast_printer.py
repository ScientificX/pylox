from abc import ABC
from .Expr import *
from lexer.lexer import Token, TokenType

class ASTPrinter(ExprVisitor):
    def _parenthesize(self,name, **args):
        out = ""
        out += "(" + " " + name
        for expr in args:
            print("o")
            out += expr.accept(self)
        out += " " + ")"
        return out
    def printExpression(self, expr):
        return expr.accept(self)

    def visitBinaryExpr(self, expr):
        return self._parenthesize(expr.op.literal, expr.left, expr.right)

    def visitGroupingExpr(self, expr):
        return self._parenthesize("Group", expr.expression)

    def visitLiteralExpr(self, expr):
        if expr.value == None: return "null"
        return expr.literal

    def visitUnaryExpr(self, expr):
        return self._parenthesize(expr.op, expr.right)


if __name__ == "__main__":
    new_expression = Binary(
        left=Literal(120),
        op=Token(TokenType.T_Plus, "+",0),
        right=Literal(31))
    f = ASTPrinter()
    print(f.printExpression(new_expression))
    # print(new_expression.op.literal)
    # print("Otiger")
