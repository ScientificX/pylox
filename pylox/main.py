#!/usr/bin/env python3


import argparse
import lexer.lexer as lexer, interp.interp as interp, pyser.pyser as pyser

ap = argparse.ArgumentParser()
ap.add_argument(
    "-p", "--path", help="Please enter path to pylox prorgam", required=False
)
args = vars(ap.parse_args())


def pipeline(source: str) -> None:
    tokens = lexer.Lexer(source)
    ast = pyser.Parse(tokens)
    interp.execute(ast)


if __name__ == "__main__":

    def main():
        if args["path"]:
            try:
                path_to_source = args["path"]
                f = open(path_to_source, "r")
                source = f.read()
                pipeline(source)
            except Exception as e:
                print(f"Ensure you are providing a correct file path {e}")
        else:
            while True:
                print("\nPylox Proramming Language v0.0.1\n\n>>>>", end=' ')
                expr = str(input())
                l = lexer.Lexer(expr)
                tokens = l.get_all_tokens()
                # for tok in tokens:
                #     print(tok)
                print((tokens))
                p = pyser.Pyser(tokens)
                print(p.parse())
    main()

